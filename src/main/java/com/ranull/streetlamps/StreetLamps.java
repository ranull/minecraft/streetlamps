package com.ranull.streetlamps;

import com.ranull.streetlamps.events.Events;
import com.ranull.streetlamps.commands.StreetLampsCommand;
import com.ranull.streetlamps.data.DataManager;
import com.ranull.streetlamps.light.LampManager;
import org.bukkit.plugin.java.JavaPlugin;

public class StreetLamps extends JavaPlugin {
	@Override
	public void onEnable() {
		DataManager dataManager = new DataManager(this);
		LampManager lampManager = new LampManager(this, dataManager);

		dataManager.createDataFile();

		saveDefaultConfig();

		lampManager.checkTimer();

		getCommand("streetlamps").setExecutor(new StreetLampsCommand(this));

		getServer().getPluginManager().registerEvents(new Events(this, lampManager, dataManager), this);
	}
}