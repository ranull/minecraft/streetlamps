package com.ranull.streetlamps.events;

import com.ranull.streetlamps.StreetLamps;
import com.ranull.streetlamps.data.DataManager;
import com.ranull.streetlamps.light.LampManager;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

public class Events implements Listener {
	private StreetLamps plugin;
	private DataManager data;
	private LampManager lamp;

	public Events(StreetLamps plugin, LampManager light, DataManager data) {
		this.plugin = plugin;
		this.lamp = light;
		this.data = data;
	}

	@EventHandler
	public void placeStreetLight(BlockPlaceEvent event) {
		if (!event.getPlayer().hasPermission("streetlamps.place")) {
			return;
		}
		if (plugin.getConfig().getBoolean("settings.shift")) {
			if (!event.getPlayer().isSneaking()) {
				return;
			}
		}

		if (event.getBlockPlaced().getType().equals(Material.REDSTONE_LAMP)) {
			data.saveStreetLamp(event.getBlock().getLocation());
			String placeMessage = plugin.getConfig().getString("settings.placeMessage").replace("&", "§");
			if (!placeMessage.equals("")) {
				event.getPlayer().sendMessage(placeMessage);
			}
			lamp.updateStreetLamp(event.getBlock(), lamp.getOnOff(event.getBlock().getWorld()));
		}
	}

	@EventHandler
	public void removeStreetLight(BlockBreakEvent event) {
		if (event.getBlock().getType().equals(Material.REDSTONE_LAMP)) {
			if (data.checkStreetLamp(event.getBlock().getLocation())) {
				if (!event.getPlayer().hasPermission("streetlamps.remove")) {
					event.setCancelled(true);
					return;
				}
				data.removeStreetLamp(event.getBlock().getLocation());
				String removeMessage = plugin.getConfig().getString("settings.removeMessage").replace("&", "§");
				if (!removeMessage.equals("")) {
					event.getPlayer().sendMessage(removeMessage);
				}
			}
		}
	}
}
