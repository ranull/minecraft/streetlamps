package com.ranull.streetlamps.data;

import com.ranull.streetlamps.StreetLamps;
import org.bukkit.Location;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

public class DataManager {
	private StreetLamps plugin;
	private FileConfiguration data;
	private File dataFile;

	public DataManager(StreetLamps plugin) {
		this.plugin = plugin;
	}

	public void loadData() throws IOException, InvalidConfigurationException {
		data.load(dataFile);
	}

	public FileConfiguration getData() {
		return this.data;
	}

	public void saveStreetLamp(Location loc) {
		String block = loc.getBlock().getType().toString();
		String world = loc.getWorld().getName();
		int x = loc.getBlockX();
		int y = loc.getBlockY();
		int z = loc.getBlockZ();
		data.set(world + "." + x + "_" + y + "_" + z, block);
		saveData();
	}

	public boolean checkStreetLamp(Location loc) {
		String world = loc.getWorld().getName();
		int x = loc.getBlockX();
		int y = loc.getBlockY();
		int z = loc.getBlockZ();
		String doorbell = data.getString(world + "." + x + "_" + y + "_" + z);
        return doorbell != null;
    }

	public void removeStreetLamp(Location loc) {
		String world = loc.getWorld().getName();
		int x = loc.getBlockX();
		int y = loc.getBlockY();
		int z = loc.getBlockZ();
		data.set(world + "." + x + "_" + y + "_" + z, null);
		saveData();
	}

	public void saveData() {
		try {
			data.save(dataFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void createDataFile() {
		dataFile = new File(plugin.getDataFolder(), "data.yml");
		if (!dataFile.exists()) {
			dataFile.getParentFile().mkdirs();
			try {
				dataFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		data = new YamlConfiguration();
		try {
			data.load(dataFile);
		} catch (IOException | InvalidConfigurationException e) {
			e.printStackTrace();
		}
	}
}
