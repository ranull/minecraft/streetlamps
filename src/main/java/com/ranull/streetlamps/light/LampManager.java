package com.ranull.streetlamps.light;

import com.ranull.streetlamps.StreetLamps;
import com.ranull.streetlamps.data.DataManager;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.data.Lightable;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.scheduler.BukkitRunnable;

public class LampManager {
    private StreetLamps plugin;
    private DataManager data;

    public LampManager(StreetLamps plugin, DataManager data) {
        this.plugin = plugin;
        this.data = data;
    }

    public void checkTimer() {
        new BukkitRunnable() {
            @Override
            public void run() {
                FileConfiguration saved = data.getData();
                for (String worlds : saved.getKeys(false)) {
                    for (String streetlight : saved.getConfigurationSection(worlds).getKeys(false)) {
                        String[] cords = streetlight.split("_");
                        int x = Integer.valueOf(cords[0]);
                        int y = Integer.valueOf(cords[1]);
                        int z = Integer.valueOf(cords[2]);
                        World world = plugin.getServer().getWorld(worlds);
                        if (world != null && world.isChunkLoaded(x >> 4, z >> 4) ) {
                            Block block = new Location(world, x, y, z).getBlock();
                            if (block.getType().equals(Material.REDSTONE_LAMP)) {
                                Boolean onOff = getOnOff(world);
                                updateStreetLamp(block, onOff);
                            } else {
                                data.removeStreetLamp(block.getLocation());
                            }
                        }
                    }
                }
            }
        }.runTaskTimer(plugin, 0L, 40L);
    }

    public void updateStreetLamp(Block block, Boolean toggle) {
        if (block.getType().equals(Material.REDSTONE_LAMP)) {
            Lightable lamp = (Lightable) block.getBlockData();
            lamp.setLit(toggle);
            block.setBlockData(lamp,false);
        }
    }

    public Boolean getOnOff(World world) {
        if (plugin.getConfig().getBoolean("settings.rain")) {
            if (world.hasStorm()) {
                return true;
            }
        }
        if (plugin.getConfig().getBoolean("settings.night")) {
            if (!(getTime(world))) {
                return true;
            }
        }
        if (plugin.getConfig().getBoolean("settings.day")) {
            if (getTime(world)) {
                return true;
            }
        }
        return false;
    }

    public Boolean getTime(World world) {
        return world.getTime() < 12300 || world.getTime() > 23850;
    }
}
