package com.ranull.streetlamps.commands;

import com.ranull.streetlamps.StreetLamps;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class StreetLampsCommand implements CommandExecutor {
	private StreetLamps plugin;

	public StreetLampsCommand(StreetLamps plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		String version = "1.4";
		String author = "RandomUnknown";

		if (args.length < 1) {
			sender.sendMessage(
					ChatColor.DARK_GRAY + "» " + ChatColor.GOLD + "StreetLamps " + ChatColor.GRAY + "v" + version);
			sender.sendMessage(
					ChatColor.GRAY + "/streetlamps " + ChatColor.DARK_GRAY + "-" + ChatColor.RESET + " Plugin info");
			if (sender.hasPermission("streetlamps.reload")) {
				sender.sendMessage(ChatColor.GRAY + "/streetlamps reload " + ChatColor.DARK_GRAY + "-" + ChatColor.RESET
						+ " Reload plugin");
			}
			sender.sendMessage(ChatColor.DARK_GRAY + "Author: " + ChatColor.GRAY + author);
			return true;
		}
		if (args.length == 1 && args[0].equals("reload")) {
			if (!sender.hasPermission("streetlamps.reload")) {
				sender.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.GOLD + "StreetLamps" + ChatColor.DARK_GRAY + "]"
						+ ChatColor.RESET + " No Permission!");
				return true;
			}
			plugin.reloadConfig();
			sender.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.GOLD + "StreetLamps" + ChatColor.DARK_GRAY + "]"
					+ ChatColor.RESET + " Reloaded config file!");
		}
		return true;
	}
}
